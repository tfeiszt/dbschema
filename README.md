# tfeiszt DbSchema  

### PHP library description
Simple ORM database mapping package
 
### Syntax
  * MySql
  * PostgreSQL (Coming soon)

### Supported ORM operations
  * finding a model
  * finding model collection
  * inserting a model
  * updating a model
  * deleting a model
  * limited DDL operations via mapping

### Supported DML queries
  * Select
  * Update
  * Delete
  * Insert
  * Truncate
  * Union
  * Sub-select as field
  * Sub-select as condition
  * Sub-select as source
  
### Supported DDL operations
  * Create table
  * Create index
  * Add column
  * Add index
    
### Install by composer
```php
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://tfeiszt@bitbucket.org/tfeiszt/sqlbuilder.git"
    },
    {
      "type": "vcs",
      "url":  "https://tfeiszt@bitbucket.org/tfeiszt/dbschema.git"
    }
  ],
  "require": {
    "tfeiszt/sql-builder": "dev-master",
    "tfeiszt/db-schema": "dev-master"  
  }
}
```

### Init ORM
```php
// composer autoload.php
require_once './vendor/autoload.php';

```

### Demo
https://bitbucket.org/tfeiszt/dbschema-example/src/master/

### License
MIT
