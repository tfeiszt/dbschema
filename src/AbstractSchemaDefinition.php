<?php
namespace tfeiszt\DbSchema;

use tfeiszt\DbSchema\Model\AbstractSchema;

/**
 * Class AbstractSchemaDefinition
 * @package tfeiszt\DbSchema
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
abstract class AbstractSchemaDefinition extends AbstractSchema
{
    /**
     * Returns the registered Model class names as string array
     * [
     *      'User', 'Post', 'Comment', 'Tag'
     * ]
     * You might need to use namespaces as well. In this case:
     * [
     *      '\\namespace\\to\\User', '\\namespace\\to\\Post'
     *
     *
     * @return string[]
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    abstract public static function getRegisteredTables();

    /**
     * Returns an sql driver name. AbstractSchemaType::MYSQL by default
     * AbstractSchemaType::MYSQL Or AbstractSchemaType::POSTGRESQL
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    abstract public static function getSchemaType();

    /**
     * Returns default mysql engine. [InnoDb|MyIsam]
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getEngine()
    {
        return 'InnoDB';
    }

    /**
     * Returns default character set
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getCharacterSet()
    {
        return 'utf8';
    }

    /**
     * Returns default collate
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getCollate()
    {
        return 'utf8_unicode_ci';
    }
}
