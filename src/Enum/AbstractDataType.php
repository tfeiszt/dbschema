<?php
namespace tfeiszt\DbSchema\Enum;

/**
 * Class AbstractDataType
 * @package tfeiszt\DbSchema\Enum
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
abstract class AbstractDataType extends AbstractEnum
{
    /**
     * Primary key types
     */
    const INT_PRIMARY_KEY = 'BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT';
    const CHAR_PRIMARY_KEY = 'CHAR(20) NOT NULL PRIMARY KEY';
    const CHAR_COMPOSITE_KEY = 'CHAR(50) NOT NULL PRIMARY KEY';

    /**
     * Incremented Id types. NOT PRIMARY KEY!
     */
    const AUTOINCREMENT = 'BIGINT NOT NULL AUTO_INCREMENT';

    /**
     * String and text types
     */
    const CHAR_10 = 'VARCHAR(10)';
    const CHAR_20 = 'VARCHAR(20)';
    const CHAR_50 = 'VARCHAR(50)';
    const CHAR_100 = 'VARCHAR(100)';
    const STRING = 'VARCHAR(255)';
    const TEXT = 'TEXT';
    const MEDIUMTEXT = 'MEDIUMTEXT';
    const LONGTEXT = 'LONGTEXT';
    const JSON = 'LONGTEXT DEFAULT \'\'';

    /**
     * Standard boolean type. 0 = false, anything else is true.
     */
    const BOOL = 'TINYINT NOT NULL DEFAULT 0';

    /**
     * Integer numeric types
     */
    const TINYINT = 'TINYINT';
    const SMALLINT = 'SMALLINT';
    const INTEGER = 'INT';
    const MEDIUMINT = 'MEDIUMINT';
    const BIGINT = 'BIGINT';

    /**
     * Floating point types
     */
    const FLOAT = 'FLOAT(12,2)';
    const DOUBLE = 'DOUBLE(14,4)';
    const DOUBLE_PRECISION = 'DOUBLE PRECISION(14,4)';
    const REAL = 'REAL(10,2)';

    /**
     * Date and datetime types
     */
    const DATE = 'DATE';
    const DATE_TIME = 'DATETIME';
    const TIME = 'TIME';
    const TIMESTAMP = 'DATETIME DEFAULT CURRENT_TIMESTAMP';
    const TIMESTAMP_ON_UPDATE = 'DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP';

    /**
     * Not real mysql ENUM types. Enumeration values must be defined on ModelClass.getEnumValues()
     */
    const BOOL_ENUM = 'VARCHAR(5)';
    const CHAR_ENUM = 'VARCHAR(30)';

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getPKTypes()
    {
        return [
            AbstractDataType::INT_PRIMARY_KEY,
            AbstractDataType::CHAR_PRIMARY_KEY,
            AbstractDataType::CHAR_COMPOSITE_KEY
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getIntTypes()
    {
        return [
            AbstractDataType::AUTOINCREMENT,
            AbstractDataType::INT_PRIMARY_KEY,
            AbstractDataType::TINYINT,
            AbstractDataType::BOOL,
            AbstractDataType::SMALLINT,
            AbstractDataType::INTEGER,
            AbstractDataType::MEDIUMINT,
            AbstractDataType::BIGINT
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getFloatTypes()
    {
        return [
            AbstractDataType::FLOAT,
            AbstractDataType::DOUBLE,
            AbstractDataType::DOUBLE_PRECISION,
            AbstractDataType::REAL
        ];
    }

    /**
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getBoolTypes()
    {
        return [
            AbstractDataType::BOOL,
            AbstractDataType::BOOL_ENUM
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getEnumTypes()
    {
        return [
            AbstractDataType::BOOL_ENUM,
            AbstractDataType::CHAR_ENUM
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getStringTypes()
    {
        return [
            AbstractDataType::CHAR_ENUM,
            AbstractDataType::CHAR_COMPOSITE_KEY,
            AbstractDataType::CHAR_PRIMARY_KEY,
            AbstractDataType::CHAR_10,
            AbstractDataType::CHAR_20,
            AbstractDataType::CHAR_50,
            AbstractDataType::CHAR_100,
            AbstractDataType::STRING,
            AbstractDataType::TEXT,
            AbstractDataType::MEDIUMTEXT,
            AbstractDataType::LONGTEXT,
            AbstractDataType::JSON
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getDateTypes()
    {
        return [
            AbstractDataType::DATE,
            AbstractDataType::TIME,
            AbstractDataType::DATE_TIME,
            AbstractDataType::TIMESTAMP,
            AbstractDataType::TIMESTAMP_ON_UPDATE
        ];
    }
}
