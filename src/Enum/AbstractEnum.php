<?php
namespace tfeiszt\DbSchema\Enum;

use \ReflectionClass;

/**
 * Class AbstractEnum
 * @package tfeiszt\DbSchema\Enum
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
abstract class AbstractEnum {
    /**
     * @var null
     */
    private static $constCacheArray = NULL;

    /**
     * @param $name
     * @param bool|false $strict
     * @return bool
     */
    public static function isValidName($name, $strict = false)
    {
        $constants = self::getConstants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    /**
     * @return mixed
     */
    public static function getConstants()
    {

        if (self::$constCacheArray == NULL) {
            self::$constCacheArray = [];
        }

        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }

        return self::$constCacheArray[$calledClass];
    }

    /**
     * @param $value
     * @param bool|true $strict
     * @return bool
     */
    public static function isValidValue($value, $strict = true)
    {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict);
    }
}
