<?php
namespace tfeiszt\DbSchema\Enum;

/**
 * Class AbstractKeyType
 * @package tfeiszt\DbSchema\Enum
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
abstract class AbstractKeyType extends AbstractEnum
{
    /**
     * Key types
     */
    const PRIMARY_KEY = 'PRI';
    const FOREIGN_KEY = 'MUL';
    const UNIQUE = 'UNI';
    const NONE = '';

    /**
     * @param $str
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getKeyTypeByString($str)
    {
        switch (strtoupper($str)) {
            case 'PK':
            case 'PRI':
            case 'PRIMARY':
                return static::PRIMARY_KEY;
            case 'FK':
            case 'MUL':
            case 'FOREIGN':
                return static::FOREIGN_KEY;
            case 'UNI':
            case 'UIDX':
            case 'UNIQUE':
                return static::UNIQUE;
        }
        return static::NONE;
    }

    /**
     * @param $name
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function isUniqueIndexName($name)
    {
        $a = explode('_', $name);
        if (is_array($a)) {
            return static::getKeyTypeByString($a[0]) == static::UNIQUE;
        }
        return false;
    }

    /**
     * @param $name
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function isPrimaryKeyName($name)
    {
        $a = explode('_', $name);
        if (is_array($a)) {
            return static::getKeyTypeByString($a[0]) == static::PRIMARY_KEY;
        }
        return false;
    }

    /**
     * @param $name
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function isForeignKeyName($name)
    {
        $a = explode('_', $name);
        if (is_array($a)) {
            return static::getKeyTypeByString($a[0]) == static::FOREIGN_KEY;
        }
        return false;
    }
}
