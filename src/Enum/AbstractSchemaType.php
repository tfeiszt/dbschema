<?php
namespace tfeiszt\DbSchema\Enum;

/**
 * Class AbstractSchemaType
 * @package tfeiszt\DbSchema\Enum
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
abstract class AbstractSchemaType extends AbstractEnum
{
    /**
     * Schema types
     */
    const MYSQL = 'mysql';
    const POSTGRESQL = 'postgresql';
}
