<?php
namespace tfeiszt\DbSchema\Exception;

/**
 * Class MappingException
 * @package tfeiszt\DbSchema\Exception
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class MappingException extends \Exception
{

}
