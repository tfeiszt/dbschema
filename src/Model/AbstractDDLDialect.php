<?php
namespace tfeiszt\DbSchema\Model;

use tfeiszt\DbSchema\Model\DbColumnMeta;

/**
 * Class AbstractDDLDialect
 * @package tfeiszt\DbSchema\Model
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
abstract class AbstractDDLDialect
{
    /**
     * @param SchemaInterface $schema
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    abstract public function showTables(SchemaInterface $schema);

    /**
     * @param AbstractEntity $model
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    abstract public function showColumns(AbstractEntity $model);

    /**
     * @param AbstractEntity $model
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
   abstract  public function getColumnDefinitions(AbstractEntity $model);

    /**
     * @param AbstractEntity $model
     * @return string[]
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    abstract public function truncate(AbstractEntity $model);

    /**
     * @param AbstractEntity $model
     * @return string[]
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    abstract public function dropTable(AbstractEntity $model);

    /**
     * @param AbstractEntity $model
     * @param string $fieldName
     * @return string[]
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    abstract public function dropField(AbstractEntity $model, $fieldName);

    /**
     * @param AbstractEntity $model
     * @param string $fieldName
     * @param string|AbstractDataType $newType
     * @return string[]
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    abstract public function changeFieldType(AbstractEntity $model, $fieldName, $newType);

    /**
     * @param AbstractEntity $model
     * @param string $fieldName
     * @param string|AbstractDataType $newType
     * @return string[]
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    abstract public function addField(AbstractEntity $model, $fieldName, $newType);


    /**
     * @param MappingInterface $model
     * @param SchemaInterface $schema
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    abstract public function createTable(MappingInterface $model, SchemaInterface $schema);
}
