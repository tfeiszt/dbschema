<?php
namespace tfeiszt\DbSchema\Model;

use tfeiszt\DbSchema\Exception\MappingException;
use tfeiszt\DbSchema\Model\Field\FieldBroker;
use tfeiszt\DbSchema\Model\Field\FieldBrokerInterface;
use tfeiszt\DbSchema\Enum\AbstractDataType;

/**
 * Class AbstractEntity
 * @package tfeiszt\DbSchema\Model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
abstract class AbstractEntity implements MappingInterface, EntityInterface
{
    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * @var string
     */
    protected static $fieldBrokerClass = FieldBroker::class;

    /**
     * @var FieldBroker
     */
    protected $fieldBroker;

    /**
     * @var bool
     */
    protected $loaded = false;

    /**
     * @var array
     */
    protected $oldValues = [];


    /**
     * AbstractEntity constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        $this->getFieldBroker();
    }

    /**
     * === STATIC FUNCTIONS ===
     */

    /**
     * @param $fieldName
     * @return string|null
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getMappedFieldType($fieldName)
    {
        $fields = static::getMapping();
        $fields = array_merge($fields, static::hasMany(), static::manyToMany());
        return (isset($fields[$fieldName])) ? $fields[$fieldName] : AbstractDataType::STRING;
    }

    /**
     * @param $fieldName
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function isMappedField($fieldName)
    {
        $fields = static::getMapping();
        $fields = array_merge($fields, static::hasMany(), static::manyToMany());
        return (isset($fields[$fieldName])) ? true : false;
    }

    /**
     * @param $fieldName
     * @return mixed|null
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getDefault($fieldName)
    {
        $defaults = static::getDefaults();
        return (isset($defaults[$fieldName])) ? $defaults[$fieldName] : null;
    }

    /**
     * @param $fieldName
     * @return null
     * @throws MappingException
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getMappedEnumValues($fieldName)
    {
        if (static::isMappedField($fieldName)) {
            $fields = static::getEnumValues();
            return (isset($fields[$fieldName])) ? $fields[$fieldName] : null;
        }
        throw new MappingException("{$fieldName} is not mapped as ENUM type");
    }

    /**
     * @param $fieldName
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function isManyToMany($fieldName)
    {
        return array_key_exists($fieldName, static::manyToMany());
    }

    /**
     * Collection of ModelField type fields of getMapping()
     * [
     *      'department' => [Department]
     * ]
     *
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function hasOne()
    {
        $modelFields = [];
        $fields = static::getMapping();
        foreach ($fields as $fieldName => $type) {
            if (! AbstractDataType::isValidValue($type)) {
                $modelFields[$fieldName] = $type;
            }
        }
        return $modelFields;
    }

    /**
     * @param string $name
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function decamelize($name) {
        return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $name));
    }

    /**
     * @param $name
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function camelize($name) {
        $name = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
        $name[0] = strtolower($name[0]);
        return $name;
    }

    /**
     * === STATIC FUNCTIONS END ===
     */

    /**
     * @return \PDO
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getConnection()
    {
        return $this->pdo;
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getName()
    {
        return get_called_class();
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getShortName()
    {
        $reflect = new \ReflectionClass(get_called_class());
        return static::decamelize($reflect->getShortName());
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getSchemaName()
    {
        return static::getTablePrefix() .  lcfirst(static::getShortName());
    }

    /**
     * @param $relatedClassName
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getSchemaNameOfRelationTable($relatedClassName)
    {
        $reflect = new \ReflectionClass($relatedClassName);
        return static::getSchemaName() . '_' . lcfirst(static::decamelize($reflect->getShortName()));
    }

    /**
     * @return FieldBrokerInterface
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    protected function getFieldBroker()
    {
        if (!$this->fieldBroker) {
            $className = static::$fieldBrokerClass;
            $this->fieldBroker = new $className($this);
        }
        return $this->fieldBroker;
    }

    /**
     * @param null $fieldName
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    protected function clear($fieldName = null)
    {
        if ($fieldName === null) {
            $this->fieldBroker = null;
            $fields = static::getMapping();
            foreach ($fields as $fieldName => $type) {
                if (isset($this->{$fieldName})) {
                    unset($this->{$fieldName});
                }
            }
            $this->getFieldBroker();
        } else {
            if (isset($this->{$fieldName})) {
                unset($this->{$fieldName});
                $this->getFieldBroker()->getField($fieldName, true);
            }
        }
        $this->loaded = false;
        return $this;
    }

    /**
     * @param array $data
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    protected function load($data = [])
    {
        $this->getFieldBroker()->load($data);
        $this->loaded = true;
        return $this;
    }

    /**
     * @param array $data
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    protected function setData($data = [])
    {
        if (!$this->isLoaded()) {
            $this->load([]);
        }
        $this->getFieldBroker()->load($data, false);
        return $this;
    }


    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function isLoaded()
    {
        return $this->loaded;
    }

    /**
     * @param $name
     * @param $arguments
     * @return Field\FieldInterface
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function __call($name, $arguments)
    {
        if (static::isMappedField($name)) {
            return $this->getFieldBroker()->getField($name);
        }
    }

    /**
     * @param $name
     * @param $value
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function __set($name, $value)
    {
        if (static::isMappedField($name)) {
            $this->getFieldBroker()->getField($name)->setValue($value);
        }
    }

    /**
     * @param $name
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function __get($name)
    {
        if (static::isMappedField($name)) {
            return $this->getFieldBroker()->getField($name)->getValue();
        }
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function __toString()
    {
        if ($this->isLoaded()) {
            return $this->getFieldBroker()->getField($this->getPk())->__toString();
        }
        return '';
    }

    /**
     * @param bool $excludeLinked
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function __toArray($excludeLinked = false)
    {
        $fields = static::getMapping();
        $array = [];
        foreach ($fields as $fieldName => $type) {
            if (($excludeLinked === true) && $this->getFieldBroker()->getField($fieldName)->isLinked()) {
                continue;
            }
            $array[] = $this->getFieldBroker()->getField($fieldName)->getRawValue();
        }
        return $array;
    }

    /**
     * @param bool $excludeLinked
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function __toAssoc($excludeLinked = false)
    {
        $fields = static::getMapping();
        $array = [];
        foreach ($fields as $fieldName => $type) {
            if (($excludeLinked === true) && $this->getFieldBroker()->getField($fieldName)->isLinked()) {
                continue;
            }
            $array[static::decamelize($fieldName)] = $this->getFieldBroker()->getField($fieldName)->getRawValue();
        }
        return $array;
    }

    /**
     * @param array $data
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setFormData($data = [])
    {
        $this->setData($data);
        return $this;
    }

    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function isDirty()
    {
        $fields = static::getMapping();
        $fields = array_merge($fields, static::hasMany(), static::manyToMany());
        foreach ($fields as $fieldName => $type) {
            if ($this->getFieldBroker()->getField($fieldName)->isDirty()) {
                return true;
            }
        }
        return false;
    }
}
