<?php
namespace tfeiszt\DbSchema\Model;

/**
 * Class AbstractModel
 * @package tfeiszt\DbSchema\Model
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
abstract class AbstractModel extends AbstractEntity implements ModelInterface
{
    use DbReadTreat;
    use DbWriteTrait;

    /**
     * Creates a new instance from given data. Will not be loaded.
     *
     * @param \PDO $pdo
     * @param array $data
     * @param false $load
     * @return static
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function createFromData(\PDO $pdo, $data = [], $load = false)
    {
        $model = new static($pdo);
        if ($load === true) {
            return $model->setFormData($data);
        }
        $model->getFieldBroker()->load([]);
        $model->getFieldBroker()->load($data, false);
        return $model;
    }
}
