<?php
namespace tfeiszt\DbSchema\Model;

/**
 * Class AbstractSchema
 * @package tfeiszt\DbSchema\Model
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
abstract class AbstractSchema implements SchemaInterface
{

    use DDLTrait;

    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * AbstractEntity constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->setDialect(static::getSchemaType());
    }

    /**
     * @return \PDO
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getConnection()
    {
        return $this->pdo;
    }
}
