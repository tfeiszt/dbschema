<?php
namespace tfeiszt\DbSchema\Model;
/**
 * Class DDLStatement
 * @package tfeiszt\DbSchema\Model
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class DDLStatement
{
    /**
     * @var string
     */
    public $note;

    /**
     * @var string
     */
    public $sql;

    /**
     * @var bool
     */
    public $success = true;

    /**
     * @var string
     */
    public $error = '';

    /**
     * DDLStatement constructor.
     * @param string $note
     * @param string $sql
     */
    public function __construct($note, $sql)
    {
        $this->note = $note;
        $this->sql = $sql;
    }

    /**
     * @param $success
     * @param string $error
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setResult($success, $error = '')
    {
        $this->success = $success;
        $this->error = $error;
        return $this;
    }
}
