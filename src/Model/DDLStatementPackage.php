<?php
namespace tfeiszt\DbSchema\Model;

/**
 * Class DDLStatementPackage
 * @package tfeiszt\DbSchema\Model
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class DDLStatementPackage
{
    /**
     * @var DDLStatement[]
     */
    public $pre = [];

    /**
     * @var DDLStatement[]
     */
    public $body = [];

    /**
     * @var DDLStatement[]
     */
    public $post = [];

    /**
     * @param DDLStatement $stm
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function addToPre(DDLStatement $stm)
    {
        $this->pre[] = $stm;
        return $this;
    }

    /**
     * @param DDLStatement $stm
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function addToBody(DDLStatement $stm)
    {
        $this->body[] = $stm;
        return $this;
    }

    /**
     * @param DDLStatement $stm
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function addToPost(DDLStatement $stm)
    {
        $this->post[] = $stm;
        return $this;
    }

    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function hasPre()
    {
        return !empty($this->pre);
    }

    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function hasBody()
    {
        return !empty($this->body);
    }

    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function hasPost()
    {
        return !empty($this->post);
    }
}
