<?php
namespace tfeiszt\DbSchema\Model;

use tfeiszt\DbSchema\Enum\AbstractSchemaType;
use tfeiszt\DbSchema\Exception\MappingException;

/**
 * Trait DDLTrait
 * @package tfeiszt\DbSchema\Model
 */
trait DDLTrait
{
    /**
     * @var AbstractDDLDialect
     */
    protected $dialect;

    /**
     * @param $schemaType
     * @return $this
     * @throws MappingException
     * @throws \Exception
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    protected function setDialect($schemaType)
    {
        switch ($schemaType) {
            case AbstractSchemaType::MYSQL:
                $this->dialect = new MysqlDDLDialect();
                break;
            case AbstractSchemaType::POSTGRESQL:
                throw new \Exception('Not implemented', 501);
                break;
            default :
                throw new MappingException('Unsupported schema type [' . $schemaType . ']', 500);
                break;
        }
        return $this;
    }

    /**
     * @param array $packages
     * @return DDLScript
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    protected function makeScript($packages = [])
    {
        $script = new DDLScript($this->pdo);
        foreach ($packages as $package) {
            if (!$package->hasPre()) {
                continue;
            }
            foreach ($package->pre as $statement ) {
                $script->addStatement($statement);
            }
        }
        foreach ($packages as $package) {
            if (!$package->hasBody()) {
                continue;
            }
            foreach ($package->body as $statement ) {
                $script->addStatement($statement);
            }
        }
        foreach ($packages as $package) {
            if (!$package->hasPost()) {
                continue;
            }
            foreach ($package->post as $statement ) {
                $script->addStatement($statement);
            }
        }
        return $script;
    }

    /**
     * @param DDLStatementPackage[]
     * @return DDLScript
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getScript($statementPackages = [])
    {
        if (empty($statementPackages)) {
            return new DDLScript($this->pdo);
        }
        return $this->makeScript($statementPackages);
    }

    /**
     * @param false $generateScriptOnly
     * @return DDLScript
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function update($generateScriptOnly = false)
    {
        /**
         * 1. Get existing tables
         * 2. Get existing constraints
         * 3. Get existing indices
         * 4. Create new raw tables
         * 5. Update columns on existing tables -- supported: add, modify, -- not supported: drop
         * 6. Drop changed constraints -- not supported
         * 7. Create new and changed constraints -- supported: create new
         * 8. Drop changed indices -- not supported
         * 9. Create new and changed indices. -- supported: create new
         */

        $modelList = [];
        $packageCollection = [];
        $existingTables = $this->dialect->showTables($this);

        /**
         * New tables
         */
        foreach (static::getRegisteredTables() as $className) {
            /** @var AbstractEntity $model */
            $model = new $className($this->pdo);
            $modelList[$model::getSchemaName()] = $model;
            if (!in_array($model::getSchemaName(), $existingTables)) {
                $packageCollection[] =  $this->dialect->createTable($model, $this);
            }
        }

        /**
         * Run creation script
         */
        $script = $this->getScript($packageCollection);

        $script->execute();

        $this->log("CREATION OF NEW OBJECTS is done. " . count($script->getErrors()) . " errors of " . count($script->getStatements()) . " statements.");

        $packageCollection = [];

        /**
         * Modified tables
         */
        foreach ($modelList as $tableName => $model) {
            if (in_array($model::getSchemaName(), $existingTables)) {
                $packageCollection[] =  $this->dialect->updateTable($model, $this);
            }
        }

        /**
         * Update indexes
         */
        foreach ($modelList as $tableName => $model) {
            $packageCollection[] =  $this->dialect->updateIndices($model, $this);
        }

        /**
         * Generate update script
         */
        $script = $this->getScript($packageCollection);

        if ($generateScriptOnly === true) {
            $this->log("SQL SCRIPT HAS BEEN GENERATED");
            return $script;
        }

        /**
         * Run update script
         */
        $script->execute();

        $this->log("UPDATE OF EXISTING OBJECTS is done. " . count($script->getErrors()) . " errors of " . count($script->getStatements()) . " statements.");

        return $script;
    }


    /**
     * @param $note
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    protected function log($note)
    {
        if (php_sapi_name() == "cli") {
            echo $note . "\n";
        } else {
            echo $note . "<br>";
        }
    }
}
