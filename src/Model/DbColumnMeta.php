<?php
namespace tfeiszt\DbSchema\Model;

use tfeiszt\DbSchema\Enum\AbstractDataType;
use tfeiszt\DbSchema\Enum\AbstractKeyType;

/**
 * Class DbColumnMeta
 * @package tfeiszt\DbSchema\Model
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class DbColumnMeta
{
    public $name;

    public $type;

    public $key = '';

    public $null = true;

    public $default = null;

    public $extras = [];

    public function __construct($name, $type)
    {
        $this->name = $name;
        $this->type = strtoupper($type);
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getAbstractDataType()
    {
        // convert values here to AbstractDataType value
        return '';
    }
}
