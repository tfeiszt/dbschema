<?php
namespace tfeiszt\DbSchema\Model;

use tfeiszt\SqlBuilder\Query\SelectQuery;
use tfeiszt\SqlBuilder\SqlConditions;
use tfeiszt\SqlBuilder\ToSqlInterface;

trait DbReadTreat
{
    /**
     * @param $name
     * @param $arguments
     * @return Field\FieldInterface
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function __call($name, $arguments)
    {
        if (static::isMappedField($name)) {
            return $this->getFieldBroker()->getField($name);
        }

        $findMethod = false;
        if (0 === stripos($name, 'findFirstBy')) {
            $findMethod = 'findFirst';
        } elseif (0 === stripos($name, 'findAllBy')) {
            $findMethod = 'findAll';
        }

        if ($findMethod) {
            $replace = array('findFirstBy', 'findAllBy');
            $fields = str_ireplace($replace, '', $name);
            $fields = explode('And', $fields);

            $conditions = new SqlConditions();
            foreach ($fields as $id => $field) {
                $conditions->equal(static::decamelize($field), $arguments[$id]);
            }

            return $this->{$findMethod}($conditions);
        }
    }

    /**
     * @param array|string|null $cols
     * @return SelectQuery
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getSelect($cols = null)
    {
        if (!$cols) {
            $cols = '*';
        }
        $sql = new SelectQuery($cols);
        $sql->from(static::getSchemaName());
        return $sql;
    }

    /**
     * @param int|string $id
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function findOne($id)
    {
        $sql = new SelectQuery('*');
        $sql->from(static::getSchemaName())
            ->where()
            ->equal(static::getPk(), $id);
        $sql->limit(1);
        $query = $this->pdo->prepare($sql->toSql());
        $query->execute($sql->getArgs());
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $this->clear();
            $this->load($row);
        }
        return $this;
    }

    /**
     * @param ToSqlInterface|null $conditions
     * @param array $orderList
     * @return $this|false
     * @throws \Exception
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function findFirst(ToSqlInterface $conditions = null, $orderList = [])
    {
        if ($conditions instanceof SelectQuery) {
            $sql = $conditions;
        } elseif ($conditions instanceof SqlConditions) {
            $sql = $this->getSelect();
            if ($conditions) {
                $sql->where()->append($conditions);
            }
        } else {
            throw new \Exception('Supported condition classes [SelectQuery,SqlConditions]');
        }
        if (!empty($orderList)) {
            $sql->setOrderByList($orderList);
        } else {
            if (empty($sql->getOrderByList())) {
                $sql->setOrderByList(static::getDefaultOrderSet());
            }
        }
        $sql->limit(1);

        $query = $this->pdo->prepare($sql->toSql());
        $query->execute($sql->getArgs());
        while($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $modelClass = static::getName();
            $model = new $modelClass($this->pdo);
            $model->load($row);
            return $model;
        }
        return false;
    }

    /**
     * @param ToSqlInterface|null $conditions
     * @param null $page
     * @param null $pageSize
     * @param array $orderList
     * @throws \Exception
     * @return $this[]|bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function findAll(ToSqlInterface $conditions = null, $page = null, $pageSize = null, $orderList = [])
    {
        if ($conditions instanceof SelectQuery) {
            $sql = $conditions;
            if ($page && $pageSize) {
                $sql->page($pageSize, $page);
            }
        } elseif ($conditions instanceof SqlConditions) {
            $sql = $this->getSelect();
            if ($conditions) {
                $sql->where()->append($conditions);
            }
            $sql->page($pageSize, $page);
        } else {
            throw new \Exception('Supported condition classes [SelectQuery,SqlConditions]');
        }
        if (!empty($orderList)) {
            $sql->setOrderByList($orderList);
        } else {
            if (empty($sql->getOrderByList())) {
                $sql->setOrderByList(static::getDefaultOrderSet());
            }
        }

        $query = $this->pdo->prepare($sql->toSql());
        $query->execute($sql->getArgs());
        $collection = [];
        while($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $modelClass = static::getName();
            $model = new $modelClass($this->pdo);
            $model->load($row);
            $collection[] = $model;
        }
        return (count($collection)) ? $collection : false;
    }
}