<?php
namespace tfeiszt\DbSchema\Model;

use tfeiszt\DbSchema\Enum\AbstractDataType;
use tfeiszt\DbSchema\Enum\AbstractSchemaType;
use tfeiszt\DbSchema\Model\Field\ModelCollectionFieldInterface;
use tfeiszt\DbSchema\Model\Field\ModelFieldInterface;
use tfeiszt\SqlBuilder\Query\DeleteQuery;
use tfeiszt\SqlBuilder\Query\InsertQuery;
use tfeiszt\SqlBuilder\Query\SelectQuery;
use tfeiszt\SqlBuilder\Query\UpdateQuery;
use tfeiszt\SqlBuilder\SqlConditions;

trait DbWriteTrait
{
    /**
     * @return bool|$this
     * @throws \Exception
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function save()
    {
        if ($this->isDirty()) {
            switch (static::getMappedFieldType(static::getPk())) {
                case AbstractDataType::INT_PRIMARY_KEY :
                    $newRecord = ($this->{static::getPk()} === null || $this->{static::getPk()} == 0) ? true : false;
                    break;
                default:
                    $newRecord = !$this->isLoaded();
                    break;
            }
            // Save unsaved hasOne ModelFields
            $fields = static::hasOne();
            if (count($fields)) {
                foreach ($fields as $fieldName => $type) {
                    if ($this->{$fieldName}() instanceof ModelFieldInterface
                        && $this->{$fieldName}()->isDirty()
                    ) {
                        $this->{$fieldName}()->save();
                    }
                }
            }
            if ($newRecord) {
                $sql = new InsertQuery();
                $sql->into(static::getSchemaName())
                    ->insertValues($this->__toAssoc(true));
                $query = $this->pdo->prepare($sql->toSql());
                $query->execute($sql->getArgs());
                switch (static::getMappedFieldType(static::getPk())) {
                    case AbstractDataType::INT_PRIMARY_KEY :
                        $newId = $this->insertId();
                        break;
                    default:
                        $newId = $this->{static::getPk()};
                        break;
                }
            } else {
                if ($this->isLoaded()) {
                    $sql = new UpdateQuery();
                    $sql->update(static::getSchemaName())
                        ->setValues($this->__toAssoc(true))
                        ->where()->equal(static::getPk(), $this->{static::getPk()});
                    $query = $this->pdo->prepare($sql->toSql());
                    $query->execute($sql->getArgs());
                    $newId = $this->{static::getPk()};
                } else {
                    throw new \Exception('Model is not loaded. Update is not possible.');
                }
            }
            if ($newId) {
                // Save related models
                $fields = static::hasMany();
                $fields = array_merge($fields, static::manyToMany());
                if (count($fields)) {
                    $this->{static::getPk()}()->setDefault($newId);
                    foreach ($fields as $fieldName => $type) {
                        if ($this->{$fieldName}() instanceof ModelCollectionFieldInterface && $this->{$fieldName}()->isDirty()) {
                            $this->{$fieldName}()->save();
                        }
                    }
                }
                // Run after trigger
                $record = $this->findOne($newId);
                if ($newRecord) {
                    $record->afterCreate();
                } else {
                    $record->afterUpdate();
                }
                return $record;
            } else {
                return false;
            }
        }
        return false;
    }


    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function delete()
    {
        if ($this->isLoaded()) {
            $id = $this->getFieldBroker()->getField(static::getPk())->getRawValue();
            $sql = new DeleteQuery();
            $sql->delete(static::getSchemaName())
                ->where()
                ->equal(static::getPk(), $id);
            $query = $this->pdo->prepare($sql->toSql());
            $query->execute($sql->getArgs());
            if ($this->findFirst((new SqlConditions())->equal(static::getPk(), $id)) === false) {
                static::afterDelete($id);
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @return int|0
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function insertId()
    {
        $seqName = null;
        switch (static::getSchemaType()) {
            case AbstractSchemaType::MYSQL:
                break;
            case AbstractSchemaType::POSTGRESQL:
                $seqName = strtolower(static::getSchemaName() . '_seq');
        }
        return $this->pdo->lastInsertId($seqName);
    }

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function afterCreate()
    {
        return;
    }

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function afterUpdate()
    {
        return;
    }

    /**
     * @param $id
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function afterDelete($id)
    {
        return;
    }
}
