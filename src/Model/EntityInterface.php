<?php
namespace tfeiszt\DbSchema\Model;

/**
 * Interface EntityInterface
 * @package tfeiszt\DbSchema\Model
 */
interface EntityInterface
{
    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function isLoaded();

    /**
     * @param array $data
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setFormData($data = []);

    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function isDirty();
}
