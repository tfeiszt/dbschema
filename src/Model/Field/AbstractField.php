<?php
namespace tfeiszt\DbSchema\Model\Field;

use tfeiszt\DbSchema\Model\AbstractEntity;

/**
 * Class AbstractField
 * @package tfeiszt\DbSchema\Model\Field
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
abstract class AbstractField implements FieldInterface
{
    /**
     * @var AbstractEntity
     */
    protected $entity;

    /**
     * @var
     */
    protected $name;

    /**
     * @var
     */
    public $value;

    /**
     * AbstractField constructor.
     * @param AbstractEntity $entity
     * @param $name
     */
    public function __construct(AbstractEntity $entity, $name)
    {
        $this->entity = $entity;
        $this->name = $name;
        if ($this instanceof FormattedFieldInterface) {
            $formats = ($this->entity)::getFormats();
            if ($formats && isset($formats[$this->name])) {
                $this->setFormat($formats[$this->name]);
            }
        }
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function __toString()
    {
        if (is_array($this->value)) {
            return implode(',', $this->value);
        }
        return (string) $this->value;
    }
}
