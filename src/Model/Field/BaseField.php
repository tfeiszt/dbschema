<?php
namespace tfeiszt\DbSchema\Model\Field;

/**
 * Class BaseField
 * @package tfeiszt\DbSchema\Model\Field
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class BaseField extends AbstractField
{
    /**
     * @var string|array
     */
    protected $format;

    /**
     * @var
     */
    protected $oldValue;

    /**
     * @var
     */
    protected $defaultValue;

    /**
     * @param $value
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setDefault($value)
    {
        if (is_callable($value)) {
            $value = $value();
        }
        $this->value = $value;
        $this->oldValue = $this->getRawValue();
        $this->defaultValue = $this->getRawValue();
        return $this;
    }

    /**
     * @param $value
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getValue()
    {
        return $this->getRawValue();
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getRawValue()
    {
        return isset($this->value) ? $this->value : null;
    }

    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function isDirty()
    {
        return (($this->getRawValue() !== $this->oldValue) && ($this->getRawValue() !== $this->defaultValue)) ? true : false;
    }

    /**
     * @return bool;
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function isLinked()
    {
        return ($this instanceof ModelCollectionFieldInterface) ? true : false;
    }
}
