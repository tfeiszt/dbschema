<?php
namespace tfeiszt\DbSchema\Model\Field;

/**
 * Interface BoolFieldInterface
 * @package tfeiszt\DbSchema\Model\Field
 */
interface BoolFieldInterface extends FieldInterface
{
    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getBool();
}
