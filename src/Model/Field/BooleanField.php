<?php
namespace tfeiszt\DbSchema\Model\Field;
use tfeiszt\DbSchema\Enum\AbstractDataType;

/**
 * Class BooleanField
 * @package tfeiszt\DbSchema\Model\Field
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class BooleanField extends BaseField implements BoolFieldInterface
{
    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getBool()
    {
        if ($type = ($this->entity)::getMappedFieldType($this->name)) {
            switch ($type) {
                case AbstractDataType::BOOL_ENUM :
                    $enums = ($this->entity)::getMappedEnumValues($this->name);
                    return ($enums && isset($enums[$this->getValue()]) && $enums[$this->getValue()] === true) ? true : false;
                    break;
                case AbstractDataType::BOOL:
                    return ($this->getValue() == 1) ? true : false;
                    break;
                default:
                    return ($this->getValue() === 'true') ? true : false;
                    break;
            }
        } else {
            return ($this->getValue()) ? true : false;
        }
    }
}