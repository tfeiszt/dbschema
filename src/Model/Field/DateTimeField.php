<?php
namespace tfeiszt\DbSchema\Model\Field;
use tfeiszt\DbSchema\Enum\AbstractDataType;

/**
 * Class DateTimeField
 * @package tfeiszt\DbSchema\Model\Field
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class DateTimeField extends BaseField implements FormattedFieldInterface
{
    /**
     * @param $format
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @param $value
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setValue($value)
    {
        if ($this->format && $this->format != '') {
            try {
                if ($dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $value)) {
                    $type = ($this->entity)::getMappedFieldType($this->name);
                    switch ($type) {
                        case AbstractDataType::DATE_TIME:
                        case AbstractDataType::TIMESTAMP:
                        case AbstractDataType::TIMESTAMP_ON_UPDATE:
                            $value = $dateTime->format('Y-m-d H:i:s');
                            break;
                        case AbstractDataType::DATE:
                            $value = $dateTime->format('Y-m-d');
                            break;
                        case AbstractDataType::TIME:
                            $value = $dateTime->format('H:i:s');
                            break;
                    }
                }
            } catch (\Exception $e) {
                return parent::setValue($value);
            }
        }
        return parent::setValue($value);
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getDisplayValue()
    {
        if ($this->format && $this->format != '') {
            $type = ($this->entity)::getMappedFieldType($this->name);
            switch ($type) {
                case AbstractDataType::DATE_TIME:
                case AbstractDataType::TIMESTAMP:
                case AbstractDataType::TIMESTAMP_ON_UPDATE:
                    $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $this->getRawValue());
                    return $dateTime->format($this->format);
                    break;
                case AbstractDataType::DATE:
                    $dateTime = \DateTime::createFromFormat('Y-m-d', $this->getRawValue());
                    return $dateTime->format($this->format);
                    break;
                case AbstractDataType::TIME:
                    $dateTime = \DateTime::createFromFormat('H:i:s', $this->getRawValue());
                    return $dateTime->format($this->format);
                    break;
                default:
                    return $this->__toString();
            }
        } else {
            return $this->__toString();
        }
    }
}