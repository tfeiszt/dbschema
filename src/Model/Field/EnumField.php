<?php
namespace tfeiszt\DbSchema\Model\Field;

/**
 * Class EnumField
 * @package tfeiszt\DbSchema\Model\Field
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class EnumField extends BaseField implements EnumFieldInterface
{
    /**
     * @return array|null
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getValues()
    {
        if ($items = ($this->entity)::getMappedEnumValues($this->name)) {
            return $items;
        } else {
            return [];
        }
    }
}
