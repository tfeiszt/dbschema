<?php
namespace tfeiszt\DbSchema\Model\Field;

/**
 * Interface EnumFieldInterface
 * @package tfeiszt\DbSchema\Model\Field
 */
interface EnumFieldInterface extends FieldInterface
{
    /**
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getValues();
}
