<?php
namespace tfeiszt\DbSchema\Model\Field;

use tfeiszt\DbSchema\Enum\AbstractDataType;
use tfeiszt\DbSchema\Model\AbstractEntity;

/**
 * Class FieldBroker
 * @package tfeiszt\DbSchema\Model\Field
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class FieldBroker implements FieldBrokerInterface
{
    /**
     * @var AbstractEntity
     */
    protected $entity;

    /**
     * @var array
     */
    protected $fields = [];

    /**
     * FieldBroker constructor.
     * @param AbstractEntity $entity
     */
    public function __construct(AbstractEntity $entity)
    {
        $this->entity = $entity;
        $this->init();
    }

    /**
     * @return AbstractEntity
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param $fieldName
     * @param bool $reInit
     * @return FieldInterface
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getField($fieldName, $reInit= false)
    {
        if ($reInit === true && isset($this->fields[$fieldName])) {
            unset($this->fields[$fieldName]);
        }

        if (isset($this->fields[$fieldName]) && !isset($this->fields[$fieldName]->value)) {
            unset($this->fields[$fieldName]);
        }

        if (!isset($this->fields[$fieldName])) {
            if ($type = ($this->entity)::getMappedFieldType($fieldName)) {
                switch ($type) {
                    case AbstractDataType::AUTOINCREMENT :
                    case AbstractDataType::INT_PRIMARY_KEY:
                    case AbstractDataType::CHAR_PRIMARY_KEY:
                    case AbstractDataType::CHAR_COMPOSITE_KEY:
                        $field = new BaseField($this->entity, $fieldName);
                        break;
                    case AbstractDataType::BOOL:
                    case AbstractDataType::BOOL_ENUM:
                        $field = new BooleanField($this->entity, $fieldName);
                        break;
                    case AbstractDataType::TINYINT:
                    case AbstractDataType::SMALLINT:
                    case AbstractDataType::MEDIUMINT:
                    case AbstractDataType::INTEGER:
                    case AbstractDataType::BIGINT:
                        $field = new IntegerField($this->entity, $fieldName);
                        break;
                    case AbstractDataType::FLOAT:
                    case AbstractDataType::DOUBLE_PRECISION:
                    case AbstractDataType::DOUBLE:
                    case AbstractDataType::REAL:
                        $field = new NumericField($this->entity, $fieldName);
                        break;
                    case AbstractDataType::DATE:
                    case AbstractDataType::DATE_TIME:
                    case AbstractDataType::TIMESTAMP:
                    case AbstractDataType::TIMESTAMP_ON_UPDATE:
                    case AbstractDataType::TIME:
                        $field = new DateTimeField($this->entity, $fieldName);
                        break;
                    case AbstractDataType::JSON:
                        $field = new JsonField($this->entity, $fieldName);
                        break;
                    default:
                        if (strpos($type,'[') !== false) {
                            // Model class array
                            $modelClass = substr($type, 0, strpos($type,'['));
                            $tableName = '';
                            if (($this->entity)::isManyToMany($fieldName)) {
                                $tableName = ($this->entity)::getSchemaNameOfRelationTable($modelClass);
                                $fkFieldName = lcfirst(($this->entity)::getShortName());
                            } else {
                                $fkFieldName = substr($type, strpos($type, '[') + 1, -1);
                            }
                            $field = new ModelCollectionField($this->entity, $fieldName, $modelClass, $fkFieldName, $tableName);
                        } elseif (class_exists($type, true)) {
                            $modelClass = $type;
                            $field = new ModelField($this->entity, $fieldName, $modelClass);
                        } else {
                            $field = new BaseField($this->entity, $fieldName);
                        }
                        break;
                }
            } else {
                $field = new BaseField($this->entity, $fieldName);
            }
            $this->fields[$fieldName] = $field;
        }
        return $this->fields[$fieldName];
    }

    /**
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    protected function init()
    {
        $this->fields = [];
        foreach (($this->entity)::getMapping() as $fieldName => $type) {
            $this->getField($fieldName);
            if (($this->entity)::getDefault($fieldName) !== null) {
                $this->getField($fieldName)->setDefault(($this->entity)::getDefault($fieldName));
            }
        }
        foreach (($this->entity)::hasMany() as $fieldName => $type) {
            $this->getField($fieldName);
            if (($this->entity)::getDefault($fieldName) !== null) {
                $this->getField($fieldName)->setDefault(($this->entity)::getDefault($fieldName));
            }
        }
        $this->fields[($this->entity)::getPk()]->setValue(null);
        return $this;
    }

    /**
     * @param array $data
     * @param bool $reInit
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function load($data = [], $reInit = true)
    {
        if ($reInit === true) {
            $this->init();
        }

        foreach ($data as $fieldName => $value) {
            $propertyName = ($this->entity)::camelize($fieldName);
            if (isset($this->fields[$propertyName]) && $this->fields[$propertyName] instanceof FieldInterface) {
                if ($reInit === true) {
                    $this->fields[$propertyName]->setDefault($value);
                } else {
                    $this->fields[$propertyName]->setValue($value);
                }
            }
        }
        return $this;
    }
}
