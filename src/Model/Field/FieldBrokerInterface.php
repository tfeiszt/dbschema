<?php
namespace tfeiszt\DbSchema\Model\Field;
/**
 * Interface FieldInterface
 * @package tfeiszt\DbSchema\Model\Field
 */
interface FieldBrokerInterface
{
    /**
     * @param $fieldName
     * @param false $reInit
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getField($fieldName, $reInit= false);

    /**
     * @param array $data
     * @return FieldBrokerInterface
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function load($data = [], $reInit = true);
}
