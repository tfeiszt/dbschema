<?php
namespace tfeiszt\DbSchema\Model\Field;
/**
 * Interface FieldInterface
 * @package tfeiszt\DbSchema\Model\Field
 */
interface FieldInterface
{
    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getValue();

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getRawValue();

    /**
     * @param $value
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setDefault($value);

    /**
     * @param $value
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setValue($value);

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function __toString();

    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function isDirty();

    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function isLinked();
}
