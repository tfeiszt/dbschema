<?php
namespace tfeiszt\DbSchema\Model\Field;

/**
 * Interface FormattedFieldInterface
 * @package tfeiszt\DbSchema\Model\Field
 */
interface FormattedFieldInterface extends FieldInterface
{
    /**
     * @param string|array $format
     * @return FormattedFieldInterface
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setFormat($format);

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getDisplayValue();
}
