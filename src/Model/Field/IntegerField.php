<?php
namespace tfeiszt\DbSchema\Model\Field;

/**
 * Class IntegerField
 * @package tfeiszt\DbSchema\Model\Field
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class IntegerField extends BaseField implements FormattedFieldInterface
{
    /**
     * @param $format
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getDisplayValue()
    {
        if (isset($this->format['decimals'])) {
            $decimals = $this->format['decimals'];
        } else {
            $decimals = 0;
        }
        $decSep = isset($this->format['decimal_separator']) ? $this->format['decimal_separator'] : '.';
        $thousandSep = isset($this->format['thousands_separator']) ? $this->format['thousands_separator'] : ',';
        return number_format($this->getValue(), $decimals, $decSep, $thousandSep);
    }
}
