<?php
namespace tfeiszt\DbSchema\Model\Field;

/**
 * Class JsonField
 * @package tfeiszt\DbSchema\Model\Field
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class JsonField extends BaseField
{
    /**
     * @param mixed $value
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setValue($value)
    {
        if (is_string($value)) {
            $value = json_decode($value);
        }
        $this->value = $value;
        if ($this->oldValue === null) {
            $this->oldValue = $value;
        }
        return $this;
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getValue()
    {
        if (is_string($this->value)) {
            $this->setValue($this->value); // forced convert to json object
        }
        return $this->value;
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getRawValue()
    {
        return json_encode($this->getValue());
    }
}
