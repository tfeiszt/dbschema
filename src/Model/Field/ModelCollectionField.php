<?php
namespace tfeiszt\DbSchema\Model\Field;
use tfeiszt\DbSchema\Model\AbstractEntity;
use tfeiszt\SqlBuilder\Query\DeleteQuery;
use tfeiszt\SqlBuilder\Query\InsertQuery;
use tfeiszt\SqlBuilder\Query\SelectQuery;
use tfeiszt\SqlBuilder\SqlConditions;
use tfeiszt\SqlBuilder\Syntax\SqlString;

/**
 * Class ModelCollectionField
 * @package tfeiszt\DbSchema\Model\Field
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class ModelCollectionField extends BaseField implements ModelCollectionFieldInterface
{
    /**
     * @var string
     */
    protected $modelClass;

    /**
     * @var string
     */
    protected $fkFieldName;

    /**
     * @var string
     */
    protected $relationTable;

    /**
     * @var array
     */
    protected $removed = [];

    /**
     * ModelField constructor.
     * @param AbstractEntity $entity
     * @param string $name
     * @param string $modelClass
     * @param string$fkFieldName
     * @param string $relationTable
     */
    public function __construct(AbstractEntity $entity, $name, $modelClass, $fkFieldName, $relationTable = '')
    {
        parent::__construct($entity, $name);
        $this->modelClass = $modelClass;
        $this->fkFieldName = $fkFieldName;
        $this->relationTable = $relationTable;
        $this->removed = [];
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getModelClass()
    {
        return $this->modelClass;
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getFkField()
    {
        return $this->fkFieldName;
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    protected function modelFactory()
    {
        $className = $this->modelClass;
        return new $className(($this->entity)->getConnection());
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getValue()
    {
        if (isset($this->value) && is_array($this->value)) {
            return $this->value;
        } else {
            $model = $this->modelFactory();
            if ($this->relationTable !== '') {
                $conditions = new SqlConditions();
                $existsSql = new SelectQuery('*');
                $existsSql->from($this->relationTable)
                    ->where()
                    ->add( lcfirst($model::getShortName()) . ' = ' . $model::getSchemaName() . '.' . ($this->entity)::decamelize($model::getPk()), [])
                    ->equal($this->getFkField(), $this->entity->{($this->entity)::getPk()});
                $conditions->exists($existsSql);
            } else {
                $conditions = (new SqlConditions())->equal($this->getFkField(), $this->entity->{($this->entity)::getPk()});
            }
            $collection = $model->findAll($conditions);
            $this->setValue($collection);
            return $this->value;
        }
    }

    /**
     * @param array $value
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setValue($value = [])
    {
        if ($value !== false) {
            $this->value = $value;
            $rawValue = [];
            foreach ($value as $model) {
                $rawValue[] = $model->{$model::getPk()};
            }
            if ($this->oldValue === null || $this->oldValue == $this->defaultValue) {
                $this->oldValue = $rawValue;
            }
        } else {
            $this->value = [];
            if ($this->oldValue === null || $this->oldValue == $this->defaultValue) {
                $this->oldValue = [];
            }
        }
        return $this;
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getRawValue()
    {
        $res = [];
        if (!(isset($this->value) && is_array($this->value))) {
            $this->getValue();
        }
        foreach ($this->value as $model) {
            $res[] =  $model->{$model::getPk()};
        }
        return $res;
    }


    /**
     * @return int
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function count()
    {
        if (!(isset($this->value) && is_array($this->value))) {
            $this->getValue();
        }
        return count($this->value);
    }


    /**
     * @return AbstractEntity
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function add()
    {
        if (!isset($this->value) || (!is_array($this->value))) {
            $this->value = [];
        }
        if ($this->oldValue === null || $this->oldValue == $this->defaultValue) {
            $this->oldValue = $this->getRawValue();
        }
        $modelInstance = $this->modelFactory();
        if ($this->relationTable === '') {
            $modelInstance->{$this->getFkField()} = $this->entity->{($this->entity)::getPk()};
        }
        $this->value[] = $modelInstance;
        return $this->value[$this->count()-1];
    }

    /**
     * @param AbstractEntity $modelInstance
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function append(AbstractEntity $modelInstance)
    {
        if (!isset($this->value) || (!is_array($this->value))) {
            $this->value = [];
        }
        if ($this->oldValue === null || $this->oldValue == $this->defaultValue) {
            $this->oldValue = $this->getRawValue();
        }
        if (!in_array($modelInstance->{$modelInstance::getPk()}, $this->getRawValue()) || !$modelInstance->{$modelInstance::getPk()}) {
            if ($this->relationTable === '') {
                $modelInstance->{$this->getFkField()}()->setValue($this->entity->{($this->entity)::getPk()});
            }
            $this->value[] = $modelInstance;
        }
        return $this;
    }

    /**
     * @param mixed $id
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function remove($id)
    {
        $index = array_search($id, $this->getRawValue());

        if ($index !== false && $this->count() > $index) {
            if ($this->oldValue === null || $this->oldValue == $this->defaultValue) {
                $this->oldValue = $this->getRawValue();
            }
            $this->removed[] = $this->value[$index];
            array_splice($this->value, $index, 1);
        }

        return $this;
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function save()
    {
        if (isset($this->value) && is_array($this->value) && ($this->isDirty() || (count($this->removed) > 0))) {
            foreach ($this->value as $model) {
                // Save newly created or modified relation models.
                if ($model->isDirty()) {
                    $model->{$this->getFkField()}()->setValue($this->entity->{($this->entity)::getPk()});
                    $model->save();
                }
                if ($this->relationTable != '') {
                    $sql = new SelectQuery('*');
                    $sql->from($this->relationTable)
                        ->where()
                        ->equal($model::getShortName(), $model->{$model::getPk()})
                        ->equal(($this->entity)::decamelize($this->getFkField()), $this->entity->{($this->entity)::getPk()});
                    $query = $this->entity->getConnection()->prepare($sql->toSql());
                    $query->execute($sql->getArgs());
                    if (!$query->rowCount()) {
                        // insert into relational table
                        $sql = new InsertQuery();
                        $sql->into($this->relationTable)
                            ->insertValues([
                                $model::getShortName() => $model->{$model::getPk()},
                                ($this->entity)::decamelize($this->getFkField()) => $this->entity->{($this->entity)::getPk()}
                            ]);
                        $query = $this->entity->getConnection()->prepare($sql->toSql());
                        $query->execute($sql->getArgs());
                    }
                }
            }

            if (count($this->removed) > 0) {
                foreach ($this->removed as $model) {
                    if ($this->relationTable != '') {
                        // delete many to many relation from relation table
                        $sql = new DeleteQuery();
                        $sql->delete($this->relationTable)
                            ->where()
                            ->equal($model::getShortName(), $model->{$model::getPk()})
                            ->equal(($this->entity)::decamelize($this->getFkField()), $this->entity->{($this->entity)::getPk()});
                        $query = $this->entity->getConnection()->prepare($sql->toSql());
                        $query->execute($sql->getArgs());
                    } else {
                        // delete one to many relation model from another fact table
                        $model->delete();
                    }
                }
            }
            unset($this->value);
            $this->getValue();
        }
        return $this;
    }
}
