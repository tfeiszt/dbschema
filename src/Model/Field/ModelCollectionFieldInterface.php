<?php
namespace tfeiszt\DbSchema\Model\Field;
use tfeiszt\DbSchema\Model\AbstractEntity;

/**
 * Interface ModelCollectionFieldInterface
 * @package tfeiszt\DbSchema\Model\Field
 */
interface ModelCollectionFieldInterface extends ModelFieldInterface
{
    /**
     * @return int
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function count();

    /**
     * @return AbstractEntity
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function add();

    /**
     * @param AbstractEntity $modelInstance
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function append(AbstractEntity $modelInstance);

    /**
     * @param mixed $id
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function remove($id);
}
