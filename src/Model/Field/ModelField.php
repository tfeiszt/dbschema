<?php
namespace tfeiszt\DbSchema\Model\Field;
use tfeiszt\DbSchema\Model\AbstractEntity;

/**
 * Class ModelField
 * @package tfeiszt\DbSchema\Model\Field
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class ModelField extends BaseField implements ModelFieldInterface
{
    /**
     * @var string
     */
    protected $modelClass;

    /**
     * ModelField constructor.
     * @param AbstractEntity $entity
     * @param $name
     * @param $modelClass
     */
    public function __construct(AbstractEntity $entity, $name, $modelClass)
    {
        parent::__construct($entity, $name);
        $this->modelClass = $modelClass;
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getModelClass()
    {
        return $this->modelClass;
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    protected function modelFactory()
    {
        $className = $this->modelClass;
        return new $className(($this->entity)->getConnection());
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getValue()
    {
        if (isset($this->value) && ($this->value instanceof AbstractEntity)) {
            return $this->value;
        } else {
            $model = $this->modelFactory();
            $this->value = $model->findOne($this->getRawValue());
            return $this->value;
        }
    }

    /**
     * @param $value
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setValue($value)
    {
        $this->value = $value;
        if ($value instanceof AbstractEntity) {
            $rawValue = $value->{$value::getPk()}()->getRawValue();
        } else {
            $rawValue = $value;
        }
        if ($this->oldValue === null || $this->oldValue == $this->defaultValue) {
            $this->oldValue = $rawValue;
        }
        return $this;
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getRawValue()
    {
        if (isset($this->value) && ($this->value instanceof AbstractEntity)) {
            return (call_user_func([$this->value, ($this->value)::getPk()]))->getRawValue();
        } else {
            return parent::getRawValue();
        }
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function save()
    {
        if (isset($this->value) && ($this->value instanceof AbstractEntity)) {
            $this->setDefault($this->value->save()->getRawValue());
        }
        return $this;
    }
}
