<?php
namespace tfeiszt\DbSchema\Model\Field;

/**
 * Interface ModelFieldInterface
 * @package tfeiszt\DbSchema\Model\Field
 */
interface ModelFieldInterface extends FieldInterface
{
    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getModelClass();

    /**
     * @return ModelFieldInterface
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function save();
}
