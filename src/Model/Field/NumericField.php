<?php
namespace tfeiszt\DbSchema\Model\Field;
use tfeiszt\DbSchema\Enum\AbstractDataType;

/**
 * Class NumericField
 * @package tfeiszt\DbSchema\Model\Field
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class NumericField extends BaseField implements FormattedFieldInterface
{
    /**
     * @param $format
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getDisplayValue()
    {
        if (isset($this->format['decimals'])) {
            $decimals = $this->format['decimals'];
        } else {
            $type = ($this->entity)::getMappedFieldType($this->name);
            switch ($type) {
                case AbstractDataType::DOUBLE:
                case AbstractDataType::DOUBLE_PRECISION:
                    $decimals = 4;
                    break;
                default:
                    $decimals = 2;
            }
        }
        $decSep = isset($this->format['decimal_separator']) ? $this->format['decimal_separator'] : '.';
        $thousandSep = isset($this->format['thousands_separator']) ? $this->format['thousands_separator'] : ',';
        return number_format($this->getRawValue(), $decimals, $decSep, $thousandSep);
    }
}