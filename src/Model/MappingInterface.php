<?php
namespace tfeiszt\DbSchema\Model;

/**
 * Interface MappingInterface
 * @package tfeiszt\DbSchema\Model
 */
interface MappingInterface
{
    /**
     * Mapping of table or a view.
     * Field names are camelCased.
     * [
     *      'id' => AbstractDataType::INT_PRIMARY_KEY,
     *      'urlName' => AbstractDataType::CHAR_50,
     *      'name' => AbstractDataType::STRING,
     *      'textDescription' => AbstractDataType::TEXT,
     *      'userRole' => AbstractDataType::SIMPLE_ENUM,
     *      'userActive' => AbstractDataType::BOOLEAN_ENUM,
     *      'department' => [Department] // One to One relation to another entity class
     *      'createdDate' => AbstractDataType::DATETIME
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getMapping();

    /**
     * Collection of ModelField type fields of getMapping()
     * [
     *      'department' => [Department]
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function hasOne();

    /**
     * One to many relations to another entity class
     * Array key is the property name on this model, value is model class. It must be marked as class array [Class[]]
     *
     * [
     *      'addresses' => '[Address[]]'
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function hasMany();

    /**
     * Many to many relations to another entity class
     * Array key is the property name on this model, value is model class. It must be marked as class array [Class[]]
     * [
     *      'tags' => '[Tag[]]'
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function manyToMany();

    /**
     * Mapping indexed fields. Not included primary key!
     * Index id is under_score_named, field name is camelCased
     * [
     *      'idx_url_name' => 'urlName',
     * ]
     *
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getIndices();

    /**
     * Mapping of fields and default values.
     * It can be value or closure.
     *
     * [
     *      'userActive' => 1,
     *      'createdDate' => function() {
     *          return Date('Y-m-d H:i:s');
     *      },
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getDefaults();

    /**
     * Mapping of fields and default display formats
     * [
     *      'createdDate' => 'd/m/Y'
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getFormats();

    /**
     * Values for enumeration field types.
     * [
     *      'userActive' => [
     *          'yes' => true,
     *          'no' => false
     *      ],
     *      'userRole' => [
     *          'user' => 'User',
     *          'admin' => 'Administrator',
     *          'super' => 'Super Admin'
     *      ]
     * ]
     *
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getEnumValues();

    /**
     * Returns table prefix if it has one, or empty string
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getTablePrefix();

    /**
     * Returns PK field camelCased name
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getPk();

    /**
     * Returns an sql driver name. AbstractSchemaType::MYSQL by default
     * AbstractSchemaType::MYSQL Or AbstractSchemaType::POSTGRESQL
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
     public static function getSchemaType();

    /**
     * Returns an order set.
     * [
     *   [ 'column_name', 'ASC' ]
     * ]
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
     public static function getDefaultOrderSet();
}