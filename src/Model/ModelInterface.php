<?php
namespace tfeiszt\DbSchema\Model;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Interface ModelInterface
 * @package tfeiszt\DbSchema\Model
 */
interface ModelInterface
{
    /**
     * @param array|string|null $cols
     * @return SelectQuery
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getSelect($cols = null);

    /**
     * @param ToSqlInterface|null $conditions
     * @param int|null $page
     * @param int|null $pageSize
     * @param  array $orderList
     * @return $this[]
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function findAll(ToSqlInterface $conditions = null, $page = null, $pageSize = null, $orderList = []);

    /**
     * @param ToSqlInterface $conditions
     * @param array $orderList
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function findFirst(ToSqlInterface $conditions = null, $orderList = []);

    /**
     * @param int|string $id
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function findOne($id);

    /**
     * @return $this
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function save();


    /**
     * @return int|false
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function delete();

    /**
     * @return int|0
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function insertId();
}
