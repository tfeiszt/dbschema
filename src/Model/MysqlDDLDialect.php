<?php
namespace tfeiszt\DbSchema\Model;

use tfeiszt\DbSchema\Enum\AbstractDataType;
use tfeiszt\DbSchema\Enum\AbstractKeyType;

/**
 * Class MysqlDDLDialect
 * @package tfeiszt\DbSchema\Model
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class MysqlDDLDialect extends AbstractDDLDialect
{
    /**
     * @param SchemaInterface $schema
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function showTables(SchemaInterface $schema)
    {
        $sql = "SHOW TABLES";
        $query = $schema->getConnection()->prepare($sql);
        $query->execute([]);
        $tables = [];
        if ($query) {
            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $tables[] = $row[array_keys($row)[0]];
            }
        }
        return $tables;
    }

    /**
     * @param AbstractEntity $model
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function showColumns(AbstractEntity $model)
    {
        return "SHOW COLUMNS FROM " . $model::getSchemaName();
    }

    /**
     * @param AbstractEntity $model
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function showIndices(AbstractEntity $model)
    {
        return "SHOW INDEX FROM ". $model::getSchemaName();
    }

    /**
     * @param AbstractEntity $model
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getColumnDefinitions(AbstractEntity $model)
    {
        $sql = $this->showColumns($model);
        $query = $model->getConnection()->prepare($sql);
        $query->execute([]);
        $columns = [];
        if ($query) {
            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $column = new DbColumnMeta($row['Field'], $row['Type']);
                $column->key = AbstractKeyType::getKeyTypeByString($row['Key']);
                $column->default = $row['Default'];
                $column->null = (strtoupper($row['Null']) === 'YES') ? true : false;
                $column->extras = $row['Extra'];
                $columns[$row['Field']] = $column;
            }
        }
        return $columns;
    }

    /**
     * @param AbstractEntity $model
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getIndexDefinitions(AbstractEntity $model)
    {
        $sql = $this->showIndices($model);
        $query = $model->getConnection()->prepare($sql);
        $query->execute([]);
        $indices = [];
        if ($query) {
            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $name =str_replace('_'. $model::getSchemaName(), '', $row['Key_name']);
                if (isset($indices[$name])) {
                    $indices[$name] .= ',' . $row['Column_name'];
                } else {
                    $indices[$name] = $row['Column_name'];
                }
            }
        }
        return $indices;
    }

    /**
     * @param AbstractEntity $model
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function truncate(AbstractEntity $model)
    {
        return "TRUNCATE " . $model::getSchemaName();
    }

    /**
     * @param AbstractEntity $model
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function dropTable(AbstractEntity $model)
    {
        return "DROP TABLE " . $model::getSchemaName();
    }

    /**
     * @param AbstractEntity $model
     * @param string $fieldName
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function dropField(AbstractEntity $model, $fieldName)
    {
        return "ALTER TABLE " . $model::getSchemaName() . " DROP COLUMN " . $fieldName;
    }

    /**
     * @param AbstractEntity $model
     * @param string $fieldName
     * @param string|AbstractDataType $newType
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function changeFieldType(AbstractEntity $model, $fieldName, $newType)
    {
        return "ALTER TABLE " . $model::getSchemaName() . " MODIFY COLUMN " . $fieldName . " " . $newType;
    }

    /**
     * @param AbstractEntity $model
     * @param string $fieldName
     * @param string|AbstractDataType $newType
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function addField(AbstractEntity $model, $fieldName, $newType)
    {
        return "ALTER TABLE " . $model::getSchemaName() . " ADD COLUMN " . $fieldName . " " . $newType;
    }

    /**
     * @param AbstractEntity $model
     * @param $name
     * @param $field
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function addIndex(AbstractEntity $model, $name, $field) {
        if (AbstractKeyType::isUniqueIndexName($name)) {
            $sql = "CREATE UNIQUE INDEX IF NOT EXISTS ";
        } else {
            $sql = "CREATE INDEX IF NOT EXISTS ";
        }
        $sql .= $name . "_" . $model::getSchemaName() . " ON " . $model::getSchemaName() . "(" . $field . ")";
        return $sql;
    }

    /**
     * @param AbstractEntity $model
     * @param $name
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function dropIndex(AbstractEntity $model, $name)
    {
        return "DROP INDEX IF EXISTS " . $name . "_" . $model::getSchemaName() . " ON " . $model::getSchemaName();
    }

    /**
     * @param $realType
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    private function getRelatedFieldType($realType) {
        if (in_array($realType, AbstractDataType::getPKTypes())) {
            if ($realType == AbstractDataType::INT_PRIMARY_KEY) {
                $realType = AbstractDataType::BIGINT;
            } elseif ($realType == AbstractDataType::CHAR_PRIMARY_KEY) {
                $realType = AbstractDataType::CHAR_20;
            } else {
                $realType = AbstractDataType::CHAR_50;
            }
        } else {
            if (in_array($realType, AbstractDataType::getIntTypes())) {
                $realType = AbstractDataType::BIGINT;
            } else {
                $realType = AbstractDataType::CHAR_50;
            }
        }
        return $realType;
    }

    /**
     * @param MappingInterface $model
     * @param SchemaInterface $schema
     * @return DDLStatementPackage
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function createTable(MappingInterface $model, SchemaInterface $schema)
    {
        /**
         * Table creation
         */
        $result = new DDLStatementPackage();
        $fields = $model::getMapping();
        $body = "";
        $sep = "";
        foreach ($fields as $name => $type) {
            $realType = $type;
            if (in_array($type, AbstractDataType::getStringTypes())) {
                $realType = $type . " COLLATE " . $schema::getCollate() ;
            } elseif (in_array($type, $schema::getRegisteredTables())) {
                $mapping = $type::getMapping();
                $realType = $this->getRelatedFieldType($mapping[$type::getPk()]);
            }
            $body .= $sep . $model::decamelize($name) . " " . $realType;
            $sep = ",\n";
        }
        $engine = "ENGINE=" . $schema::getEngine() ." DEFAULT CHARSET=" . $schema::getCharacterSet() . " COLLATE=" . $schema::getCollate();
        $sql = "CREATE TABLE " . $model::getSchemaName() . " (\n" . $body . "\n) " . $engine . ";";
        $result->addToBody(
            new DDLStatement(
                "Creating table " . $model::getSchemaName(),
                $sql
            )
        );

        $result = $this->createRelationTables($model, $schema, $result);

        return $result;
    }

    protected function createRelationTables(MappingInterface $model, SchemaInterface $schema, DDLStatementPackage &$result)
    {
        /**
         * Create relation table for many to many relations
         */
        $fields = $model::getMapping();
        $manyToManyDefinition = $model::manyToMany();
        $existingTables = $this->showTables($schema);
        if (!empty($manyToManyDefinition)) {
            $relatedField = $model::getShortName();
            $relatedFieldType = $this->getRelatedFieldType($fields[$model::getPk()]) . " NOT NULL";

            $body = $relatedField . " " . $relatedFieldType;
            $manyToManyModels = [];
            foreach ($manyToManyDefinition as $fieldName => $type) {
                $manyToManyModels[$model::getSchemaNameOfRelationTable(str_replace('[]', '', $type))] = str_replace('[]', '', $type);
            }
            foreach ($manyToManyModels as $fieldName => $manyToManyModel) {
                $relatedFieldName = $manyToManyModel::getShortName();
                if (!in_array($fieldName, $existingTables)) {
                    $mapping = $manyToManyModel::getMapping();
                    $realType = $this->getRelatedFieldType($mapping[$manyToManyModel::getPk()]) . " NOT NULL";
                    $engine = "ENGINE=" . $schema::getEngine() . " DEFAULT CHARSET=" . $schema::getCharacterSet() . " COLLATE=" . $schema::getCollate();
                    $sql = "CREATE TABLE  " . $fieldName . " (\n" . $body . ",\n" . $relatedFieldName . " " . $realType . ",\n PRIMARY KEY (" . $relatedField . ",\n" . $relatedFieldName . "))\n" . $engine . ";";
                    $result->addToBody(
                        new DDLStatement(
                            "Creating table "  . $fieldName,
                            $sql
                        )
                    );
                    /**
                     * Add foreign keys and delete cascade
                     */
                    $sql = "ALTER TABLE " . $fieldName . " ADD CONSTRAINT fk_" . $fieldName . "_" . $relatedField . " FOREIGN KEY (" . $relatedField . ") REFERENCES " . $model::getSchemaName() . "(" . $model::getPk() . ") ON DELETE CASCADE;";
                    $result->addToPost(
                        new DDLStatement(
                            "Creating foreign key " . "fk_" . $fieldName . "_" . $relatedField . " on table " . $fieldName . '(' . $relatedField . ')',
                            $sql
                        )
                    );
                    $sql = "ALTER TABLE " . $fieldName . " ADD CONSTRAINT fk_" . $fieldName . "_" . $relatedFieldName . " FOREIGN KEY (" . $relatedFieldName . ") REFERENCES " . $manyToManyModel::getSchemaName() . "(" . $manyToManyModel::getPk() . ") ON DELETE CASCADE;";
                    $result->addToPost(
                        new DDLStatement(
                            "Creating foreign key " . "fk_" . $fieldName . "_" . $relatedFieldName . " on table " . $fieldName . '(' . $relatedFieldName . ')',
                            $sql
                        )
                    );
                }
            }
        }
        return $result;
    }

    /**
     * @param MappingInterface $model
     * @param SchemaInterface $schema
     * @return DDLStatementPackage
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function updateTable(MappingInterface $model, SchemaInterface $schema)
    {
        $result = new DDLStatementPackage();
        $fields = $model::getMapping();
        $existingColumns = $this->getColumnDefinitions($model);
        foreach ($fields as $name => $type) {
            $realType = $type;
            if (in_array($type, AbstractDataType::getStringTypes())) {
                $realType = $type . " COLLATE utf8_unicode_ci";
            } elseif (in_array($type, $schema::getRegisteredTables())) {
                $mapping = $type::getMapping();
                $realType = $this->getRelatedFieldType($mapping[$type::getPk()]);
            }
            if (!array_key_exists($model::decamelize($name), $existingColumns)) {
                $sql = $this->addField($model, $name, $realType);
                $result->addToBody(
                    new DDLStatement(
                        "Adding column " . $model::getSchemaName() . "_" . $name,
                        $sql
                    )
                );
            }
        }

        $result = $this->createRelationTables($model, $schema, $result);

        return $result;
    }

    /**
     * @param MappingInterface $model
     * @param SchemaInterface $schema
     * @return DDLStatementPackage
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function updateIndices(MappingInterface $model, SchemaInterface $schema)
    {
        $result = new DDLStatementPackage();
        $indices = $model::getIndices();
        $existingIndices = $this->getIndexDefinitions($model);
        foreach ($existingIndices as $name => $field) {
            if (AbstractKeyType::getKeyTypeByString($name) != AbstractKeyType::PRIMARY_KEY && !array_key_exists($name, $indices)) {
                $result->addToPost(
                    new DDLStatement(
                        "Drop " . ((AbstractKeyType::isUniqueIndexName($name)) ? "unique " : "") . "index " . $name . "_" . $model::getSchemaName(),
                        $this->dropIndex($model, $name, $model::decamelize($field))
                    )
                );
            }
        }
        foreach ($indices as $name => $field) {
            if (array_key_exists($name, $existingIndices)) {
                if ($field != $existingIndices[$name]) {
                    $result->addToPost(
                        new DDLStatement(
                            "Drop " . ((AbstractKeyType::isUniqueIndexName($name)) ? "unique " : "") . "index " . $name . "_" . $model::getSchemaName(),
                            $this->dropIndex($model, $name, $model::decamelize($field))
                        )
                    );
                }
            }
            $result->addToPost(
                new DDLStatement(
                    "Creating " . ((AbstractKeyType::isUniqueIndexName($name)) ? "unique " : "") . "index " . $name . "_" .  $model::getSchemaName(),
                    $this->addIndex($model, $name, $model::decamelize($field))
                )
            );
        }
        return $result;
    }
}
