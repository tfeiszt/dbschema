<?php
namespace tfeiszt\DbSchema\Model;

/**
 * Interface SchemaInterface
 * @package tfeiszt\DbSchema\Model
 */
interface SchemaInterface
{
    /**
     * Returns the registered Model class names as string array
     * [
     *      'User', 'Post', 'Comment', 'Tag'
     * ]
     * You might need to use namespaces as well. In this case:
     * [
     *      '\\namespace\\to\\User', '\\namespace\\to\\Post'
     *
     *
     * @return string[]
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getRegisteredTables();

    /**
     * Returns an sql driver name. AbstractSchemaType::MYSQL by default
     * AbstractSchemaType::MYSQL Or AbstractSchemaType::POSTGRESQL
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getSchemaType();

    /**
     * Returns default mysql engine. [InnoDb|MyIsam]
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getEngine();

    /**
     * Returns default character set
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getCharacterSet();

    /**
     * Returns default collate
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getCollate();


    /**
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function update();
}
